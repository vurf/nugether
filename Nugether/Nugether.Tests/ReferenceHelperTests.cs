﻿using Nugether;
using NUnit.Framework;
using System.IO;

namespace Nugether.Tests
{
    [TestFixture()]
    public class ReferenceHelperTests
    {
        private string packagesFolder;

        private string PackageFolder
        {
            get 
            {
                if (this.packagesFolder == null)
                {
                    var nugetherTestsFolder = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    this.packagesFolder = Path.Combine(nugetherTestsFolder, "test-packages", "packages");
                }

                return this.packagesFolder;
            }
        }

        [Test()]
        public void HockeySDKXamarinDroid()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "HockeySDK.Xamarin", Platform.Android);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual("HockeySDK.AndroidBindings", result[0].Reference);
            Assert.AreEqual(@"packages\HockeySDK.Xamarin.5.0.0\lib\MonoAndroid403\HockeySDK.AndroidBindings.dll", result[0].HintPath);

            Assert.AreEqual("HockeySDK", result[1].Reference);
            Assert.AreEqual(@"packages\HockeySDK.Xamarin.5.0.0\lib\MonoAndroid403\HockeySDK.dll", result[1].HintPath);
        }

        [Test()]
        public void HockeySDKXamarinIos()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "HockeySDK.Xamarin", Platform.iOS);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual("HockeySDK", result[0].Reference);
            Assert.AreEqual(@"packages\HockeySDK.Xamarin.5.0.0\lib\Xamarin.iOS10\HockeySDK.dll", result[0].HintPath);

            Assert.AreEqual("HockeySDK.iOSBindings", result[1].Reference);
            Assert.AreEqual(@"packages\HockeySDK.Xamarin.5.0.0\lib\Xamarin.iOS10\HockeySDK.iOSBindings.dll", result[1].HintPath);
        }

        [Test]
        public void AcrUserDialogsIos()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "Acr.UserDialogs", Platform.iOS);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("Acr.UserDialogs", result[0].Reference);
            Assert.AreEqual(@"packages\Acr.UserDialogs.7.0.1\lib\xamarinios10\Acr.UserDialogs.dll", result[0].HintPath);
        }

        [Test]
        public void AcrUserDialogsDroid()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "Acr.UserDialogs", Platform.Android);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("Acr.UserDialogs", result[0].Reference);
            Assert.AreEqual(@"packages\Acr.UserDialogs.7.0.1\lib\monoandroid80\Acr.UserDialogs.dll", result[0].HintPath);
        }

        [Test]
        public void FluentValidationIos()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "FluentValidation", Platform.iOS);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("FluentValidation", result[0].Reference);
            Assert.AreEqual(@"packages\FluentValidation.7.6.100\lib\netstandard2.0\FluentValidation.dll", result[0].HintPath);
        }

        [Test]
        public void FluentValidationDroid()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "FluentValidation", Platform.Android);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("FluentValidation", result[0].Reference);
            Assert.AreEqual(@"packages\FluentValidation.7.6.100\lib\netstandard2.0\FluentValidation.dll", result[0].HintPath);
        }

        [Test]
        public void IdeineModernHttpClientIos()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "Ideine.ModernHttpClient", Platform.iOS);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("ModernHttpClient", result[0].Reference);
            Assert.AreEqual(@"packages\Ideine.ModernHttpClient.3.2.2\lib\Xamarin.iOS10\ModernHttpClient.dll", result[0].HintPath);
        }

        [Test]
        public void IdeineModernHttpClientDroid()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "Ideine.ModernHttpClient", Platform.Android);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("ModernHttpClient", result[0].Reference);
            Assert.AreEqual(@"packages\Ideine.ModernHttpClient.3.2.2\lib\MonoAndroid\ModernHttpClient.dll", result[0].HintPath);
        }

        [Test]
        public void JASidePaneliOS()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "JASidePanel.Navigation.iOS", Platform.iOS);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("JASidePanel.Navigation.iOS", result[0].Reference);
            Assert.AreEqual(@"packages\JASidePanel.Navigation.iOS.1.0.117-Feature-14677\lib\Xamarin.iOS10\JASidePanel.Navigation.iOS.dll", result[0].HintPath);
        }

        [Test]
        public void JASidePaneliOSDroid()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "JASidePanel.Navigation.iOS", Platform.Android);

            CollectionAssert.IsEmpty(result);
        }

        [Test]
        public void LoymaxCoreIos()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "Loymax.Core", Platform.iOS);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual("Loymax.Core", result[0].Reference);
            Assert.AreEqual(@"packages\Loymax.Core.1.0.117-Feature-14677\lib\Xamarin.iOS10\Loymax.Core.dll", result[0].HintPath);

            Assert.AreEqual("Loymax.Core.iOS", result[1].Reference);
            Assert.AreEqual(@"packages\Loymax.Core.1.0.117-Feature-14677\lib\Xamarin.iOS10\Loymax.Core.iOS.dll", result[1].HintPath);
        }

        [Test]
        public void LoymaxCoreDroid()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "Loymax.Core", Platform.Android);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual("Loymax.Core", result[1].Reference);
            Assert.AreEqual(@"packages\Loymax.Core.1.0.117-Feature-14677\lib\MonoAndroid\Loymax.Core.dll", result[1].HintPath);

            Assert.AreEqual("Loymax.Core.Droid", result[0].Reference);
            Assert.AreEqual(@"packages\Loymax.Core.1.0.117-Feature-14677\lib\MonoAndroid\Loymax.Core.Droid.dll", result[0].HintPath);
        }

        [Test]
        [TestCase(Platform.Android, TestName = "Android")]
        [TestCase(Platform.iOS, TestName = "Ios")]
        public void LoymaxMobileClientSDK(Platform platform)
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "Loymax.Mobile.ClientSDK", platform);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(8, result.Count);

            Assert.AreEqual("Loymax.Business.Announcement.Model", result[0].Reference);
            Assert.AreEqual(@"packages\Loymax.Mobile.ClientSDK.2018.0.0.11353-Story-16523\lib\portable45-net45+win8+wp8+wpa81\Loymax.Business.Announcement.Model.dll", result[0].HintPath);

            Assert.AreEqual("Loymax.Business.Social.Common", result[1].Reference);
            Assert.AreEqual(@"packages\Loymax.Mobile.ClientSDK.2018.0.0.11353-Story-16523\lib\portable45-net45+win8+wp8+wpa81\Loymax.Business.Social.Common.dll", result[1].HintPath);

            Assert.AreEqual("Loymax.Common.Contract", result[2].Reference);
            Assert.AreEqual(@"packages\Loymax.Mobile.ClientSDK.2018.0.0.11353-Story-16523\lib\portable45-net45+win8+wp8+wpa81\Loymax.Common.Contract.dll", result[2].HintPath);

            Assert.AreEqual("Loymax.Common.Portable", result[3].Reference);
            Assert.AreEqual(@"packages\Loymax.Mobile.ClientSDK.2018.0.0.11353-Story-16523\lib\portable45-net45+win8+wp8+wpa81\Loymax.Common.Portable.dll", result[3].HintPath);

            Assert.AreEqual("Loymax.History.Mobile.Contract", result[4].Reference);
            Assert.AreEqual(@"packages\Loymax.Mobile.ClientSDK.2018.0.0.11353-Story-16523\lib\portable45-net45+win8+wp8+wpa81\Loymax.History.Mobile.Contract.dll", result[4].HintPath);

            Assert.AreEqual("Loymax.Mobile.ClientSDK", result[5].Reference);
            Assert.AreEqual(@"packages\Loymax.Mobile.ClientSDK.2018.0.0.11353-Story-16523\lib\portable45-net45+win8+wp8+wpa81\Loymax.Mobile.ClientSDK.dll", result[5].HintPath);

            Assert.AreEqual("Loymax.Mobile.Contract", result[6].Reference);
            Assert.AreEqual(@"packages\Loymax.Mobile.ClientSDK.2018.0.0.11353-Story-16523\lib\portable45-net45+win8+wp8+wpa81\Loymax.Mobile.Contract.dll", result[6].HintPath);

            Assert.AreEqual("Loymax.Support.Mobile.Contract", result[7].Reference);
            Assert.AreEqual(@"packages\Loymax.Mobile.ClientSDK.2018.0.0.11353-Story-16523\lib\portable45-net45+win8+wp8+wpa81\Loymax.Support.Mobile.Contract.dll", result[7].HintPath);
        }

        [Test]
        public void MvvmCrossCoreDroid()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "MvvmCross.Core", Platform.Android);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual("MvvmCross.Core", result[0].Reference);
            Assert.AreEqual(@"packages\MvvmCross.Core.5.7.0\lib\MonoAndroid\MvvmCross.Core.dll", result[0].HintPath);

            Assert.AreEqual("MvvmCross.Droid", result[1].Reference);
            Assert.AreEqual(@"packages\MvvmCross.Core.5.7.0\lib\MonoAndroid\MvvmCross.Droid.dll", result[1].HintPath);
        }

        [Test]
        public void MvvmCrossPluginFileDroid()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "MvvmCross.Plugin.File", Platform.Android);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual("MvvmCross.Plugins.File", result[1].Reference);
            Assert.AreEqual(@"packages\MvvmCross.Plugin.File.5.7.0\lib\MonoAndroid\MvvmCross.Plugins.File.dll", result[1].HintPath);

            Assert.AreEqual("MvvmCross.Plugins.File.Droid", result[0].Reference);
            Assert.AreEqual(@"packages\MvvmCross.Plugin.File.5.7.0\lib\MonoAndroid\MvvmCross.Plugins.File.Droid.dll", result[0].HintPath);
        }

        [Test]
        public void MvvmCrossPluginFileIos()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "MvvmCross.Plugin.File", Platform.iOS);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual("MvvmCross.Plugins.File", result[0].Reference);
            Assert.AreEqual(@"packages\MvvmCross.Plugin.File.5.7.0\lib\Xamarin.iOS10\MvvmCross.Plugins.File.dll", result[0].HintPath);

            Assert.AreEqual("MvvmCross.Plugins.File.iOS", result[1].Reference);
            Assert.AreEqual(@"packages\MvvmCross.Plugin.File.5.7.0\lib\Xamarin.iOS10\MvvmCross.Plugins.File.iOS.dll", result[1].HintPath);
        }

        [Test]
        [TestCase(Platform.Android, TestName = "Android")]
        [TestCase(Platform.iOS, TestName = "Ios")]
        public void RealmDatabase(Platform platform)
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "Realm.Database", platform);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("Realm", result[0].Reference);
            Assert.AreEqual(@"packages\Realm.Database.3.0.0\lib\netstandard1.4\Realm.dll", result[0].HintPath);
        }

        [Test]
        [TestCase(Platform.Android, TestName = "Android")]
        [TestCase(Platform.iOS, TestName = "Ios")]
        public void SystemLinq(Platform platform)
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "System.Linq", platform);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("System.Linq", result[0].Reference);
            Assert.AreEqual(@"packages\System.Linq.4.0.0\lib\dotnet\System.Linq.dll", result[0].HintPath);
        }

        [Test]
        public void XamPluginConnectivityIos()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "Xam.Plugin.Connectivity", Platform.iOS);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual("Plugin.Connectivity.Abstractions", result[0].Reference);
            Assert.AreEqual(@"packages\Xam.Plugin.Connectivity.2.2.12\lib\Xamarin.iOS10\Plugin.Connectivity.Abstractions.dll", result[0].HintPath);

            Assert.AreEqual("Plugin.Connectivity", result[1].Reference);
            Assert.AreEqual(@"packages\Xam.Plugin.Connectivity.2.2.12\lib\Xamarin.iOS10\Plugin.Connectivity.dll", result[1].HintPath);
        }

        [Test]
        public void XamPluginConnectivityDroid()
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "Xam.Plugin.Connectivity", Platform.Android);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual("Plugin.Connectivity.Abstractions", result[0].Reference);
            Assert.AreEqual(@"packages\Xam.Plugin.Connectivity.2.2.12\lib\MonoAndroid10\Plugin.Connectivity.Abstractions.dll", result[0].HintPath);

            Assert.AreEqual("Plugin.Connectivity", result[1].Reference);
            Assert.AreEqual(@"packages\Xam.Plugin.Connectivity.2.2.12\lib\MonoAndroid10\Plugin.Connectivity.dll", result[1].HintPath);
        }

        [Test]
        [TestCase(Platform.Android, TestName = "Android")]
        [TestCase(Platform.iOS, TestName = "Ios")]
        public void XamarinYamlLocalization(Platform platform)
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "Xamarin.Yaml.Localization", platform);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("Xamarin.Yaml.Localization", result[0].Reference);
            Assert.AreEqual(@"packages\Xamarin.Yaml.Localization.1.1.9\lib\netstandard2.0\Xamarin.Yaml.Localization.dll", result[0].HintPath);
        }

        [Test]
        [TestCase(Platform.Android, TestName = "Android")]
        [TestCase(Platform.iOS, TestName = "Ios")]
        public void YamlDotNet(Platform platform)
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "YamlDotNet", platform);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("YamlDotNet", result[0].Reference);
            Assert.AreEqual(@"packages\YamlDotNet.5.0.1\lib\netstandard1.3\YamlDotNet.dll", result[0].HintPath);
        }

        [Test]
        [TestCase(Platform.Android, TestName = "Android")]
        [TestCase(Platform.iOS, TestName = "Ios")]
        public void SystemCollectionsImmutable(Platform platform)
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "System.Collections.Immutable", platform);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("System.Collections.Immutable", result[0].Reference);
            Assert.AreEqual(@"packages\System.Collections.Immutable.1.1.37\lib\dotnet\System.Collections.Immutable.dll", result[0].HintPath);
        }

        [Test]
        [TestCase(Platform.Android, TestName = "Android")]
        [TestCase(Platform.iOS, TestName = "Ios")]
        public void NewtonsoftJson(Platform platform)
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "Newtonsoft.Json", platform);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("Newtonsoft.Json", result[0].Reference);
            Assert.AreEqual(@"packages\Newtonsoft.Json.11.0.2\lib\netstandard2.0\Newtonsoft.Json.dll", result[0].HintPath);
        }

        [Test]
        [TestCase(Platform.Android, TestName = "Android")]
        [TestCase(Platform.iOS, TestName = "Ios")]
        public void Polly(Platform platform)
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "Polly", platform);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("Polly", result[0].Reference);
            Assert.AreEqual(@"packages\Polly.4.3.0\lib\dotnet\Polly.dll", result[0].HintPath);
        }

        [Test]
        [TestCase(Platform.Android, TestName = "Android")]
        [TestCase(Platform.iOS, TestName = "Ios")]
        public void RemotionLinq(Platform platform)
        {
            var result = ReferenceUtils.GetReferences(this.PackageFolder, "Remotion.Linq", platform);

            CollectionAssert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual("Remotion.Linq", result[0].Reference);
            Assert.AreEqual(@"packages\Remotion.Linq.2.1.2\lib\portable-net45+win+wpa81+wp80\Remotion.Linq.dll", result[0].HintPath);
        }
    }
}
