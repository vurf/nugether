﻿using System.Collections.Generic;
using System.IO;
using Nugether.Commands;
using Nugether.Models;
using NUnit.Framework;

namespace Nugether.Tests
{
    [TestFixture]
    public class NugetWriterTests
    {
        [Test]
        public void WriteReferencesToProject()
        {
            var path = @"/Users/i.varfolomeev/Desktop/sandbox/lmx-mlc/AlfaClick";
            var packagesFolder = Path.Combine(path, "packages");
            var droidCsproj = Path.Combine(path, "AlfaClick.Droid", "AlfaClick.Droid.csproj");
            var packages = new List<Package>
            {
                new Package { Id = "Acr.UserDialogs", Platfrom = Platform.Android },
                new Package { Id = "Acr.UserDialogs", Platfrom = Platform.iOS },
            };

            var command = new EditCsprojCommand();
            command.Execute(droidCsproj, packagesFolder, packages);
        }
    }
}
