﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommandLine;
using Nugether.Models;
using Nugether.Commands;

namespace Nugether
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Parser.Default.ParseArguments<CmdOptions>(args)
                  .WithParsed<CmdOptions>(opts => RunOptionsAndReturnExitCode(opts))
                  .WithNotParsed<CmdOptions>((errs) => HandleParseError(errs));
        }

        private static void RunOptionsAndReturnExitCode(CmdOptions opts)
        {
            var options = NugetOptions.Map(opts);

            //1.
            var packages = GetDependencies(options);

            //2.
            EditPackagesConfig(options, packages);

            //3.
            RestorePackages(options);

            //4.
            EditReferencesCsproj(options, packages);
        }

        private static void HandleParseError(IEnumerable<Error> errors)
        {
            if (errors != null && errors.Any())
                foreach (var error in errors)
                    Console.WriteLine(error);
        }

        private static IList<Package> GetDependencies(NugetOptions options)
        {
            if (options.Verbose)
                Console.WriteLine("Получаю зависимости пакета");
            
            var command = new GetDependenciesCommand();
            command.SetBlacklist(Blacklist.GetBlacklist());
            var packages = command.Execute(options);

            if (options.Verbose)
                Console.WriteLine("Закончил получать зависимости пакета");

            return packages;
        }

        private static void EditPackagesConfig(NugetOptions options, IList<Package> packages)
        {
            if (options.Verbose)
                Console.WriteLine("Редактирую packages.config");
            
            var project = Path.GetFileName(options.SolutionPath);
            var droidPackagesConfig = Path.Combine(options.SolutionPath, $"{project}.Droid", "packages.config");
            var iosPackagesConfig = Path.Combine(options.SolutionPath, $"{project}.iOS", "packages.config");

            var command = new EditConfigCommand();
            command.Execute(droidPackagesConfig, packages.Where(x => x.Platfrom == Platform.Android).ToList());
            command.Execute(iosPackagesConfig, packages.Where(x => x.Platfrom == Platform.iOS).ToList());

            if (options.Verbose)
                Console.WriteLine("Закончил редактировать packages.config");
        }

        private static void RestorePackages(NugetOptions options)
        {
            if (options.Verbose)
                Console.WriteLine("Восстанавливаю пакеты");
            
            var project = Path.GetFileName(options.SolutionPath);
            var solution = Path.Combine(options.SolutionPath, $"{project}.sln");

            var command = new RestoreCommand();
            command.Execute(solution);

            if (options.Verbose)
                Console.WriteLine("Закончил восстанавливать пакеты");
        }

        private static void EditReferencesCsproj(NugetOptions options, IList<Package> packages)
        {
            if (options.Verbose)
                Console.WriteLine("Восстанавливаю ссылки в файле проекта");
            
            var project = Path.GetFileName(options.SolutionPath);
            var droidPackagesConfig = Path.Combine(options.SolutionPath, $"{project}.Droid", "packages.config");
            var iosPackagesConfig = Path.Combine(options.SolutionPath, $"{project}.iOS", "packages.config");
            var droidProject = Path.Combine(options.SolutionPath, $"{project}.Droid", $"{project}.Droid.csproj");
            var iosProject = Path.Combine(options.SolutionPath, $"{project}.iOS", $"{project}.iOS.csproj");

            var command = new EditCsprojCommand();
            command.Execute(droidProject, droidPackagesConfig, packages.Where(x => x.Platfrom == Platform.Android).ToList());
            command.Execute(iosProject, iosPackagesConfig, packages.Where(x => x.Platfrom == Platform.iOS).ToList());

            if (options.Verbose)
                Console.WriteLine("Закончил восстанавливать ссылки в файле проекта");
        }
    }
}