﻿using System.Collections.Generic;

namespace Nugether
{
    public class Blacklist
    {
        public static IList<string> GetBlacklist()
        {
            return new List<string>()
            {
                "Microsoft.CSharp",
                "Microsoft.Win32.Primitives",
                "System.AppContext",
                "System.ComponentModel.TypeConverter",
                "System.Console",
                "System.Diagnostics.Tools",
                "System.Diagnostics.Tracing",
                "System.Globalization.Calendars",
                "System.IO",
                "System.IO.Compression",
                "System.IO.Compression.ZipFile",
                "System.IO.FileSystem",
                "System.IO.FileSystem.Primitives",
                "System.Linq.Expressions",
                "System.Linq.Queryable",
                "System.Net.Http",
                "System.Net.Primitives",
                "System.Net.Sockets",
                "System.ObjectModel",
                "System.Reflection",
                "System.Reflection.Extensions",
                "System.Reflection.Primitives",
                "System.Runtime.Handles",
                "System.Runtime.InteropServices",
                "System.Runtime.InteropServices.RuntimeInformation",
                "System.Runtime.Numerics",
                "System.Runtime.Serialization.Formatters",
                "System.Runtime.Serialization.Primitives",
                "System.Security.Cryptography.Algorithms",
                "System.Security.Cryptography.Encoding",
                "System.Security.Cryptography.Primitives",
                "System.Security.Cryptography.X509Certificates",
                "System.Text.Encoding",
                "System.Text.Encoding.Extensions",
                "System.Text.RegularExpressions",
                "System.Threading.Tasks",
                "System.Threading.Timer",
                "System.Xml.ReaderWriter",
                "System.Xml.XDocument",
                "System.Xml.XmlDocument",
            };
        }
    }
}
