﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Nugether.Models;

namespace Nugether
{
    public class ReferenceUtils
    {
        // Приоритет важен
        private static string[] AndroidAliases = 
        {
            "monoandroid80",
            "MonoAndroid",
        };

        private static string[] IosAliases = 
        {
            "xamarinios10",
            "Xamarin.iOS",
        };

        private static string[] PortableAliases = 
        {
            "netstandard2.0",
            "netstandard1.6",
            "netstandard1.4",
            "netstandard1.3",
            "netstandard1.1",
            "dotnet",
            "portable45",
            "portable",
            "net46",
            "net45",
        };

        /// <summary>
        /// Получить ссылки на библиотеки для csproj проекта на основе пакетов внутри папки packages.
        /// Необходимо заранее восстановить пакеты.
        /// </summary>
        /// <returns>Список ссылок на dll для проекта.</returns>
        /// <param name="path">Путь к папке packages.</param>
        /// <param name="packageId">Идентификатор пакета.</param>
        /// <param name="platform">Принадлежность пакета к платформе.</param>
        public static List<ReferenceProject> GetReferences(string path, string packageId, Platform platform)
        {
            try
            {
                // директория packages
                var allPackages = Directory.GetDirectories(path);
                allPackages = allPackages.OrderBy(x => x).ToArray();

                // из за версии пакетов, нужно брать последнюю папку (последняя версия)
                var packageRootFolder = allPackages.LastOrDefault(x => x.Contains(packageId));

                // директория внутри пакета (lib)
                var packageLibFolder = Directory.GetDirectories(packageRootFolder).FirstOrDefault(x => x.Contains("lib"));

                // директории платформ внутри папки lib
                var packagePlatformFolders = Directory.GetDirectories(packageLibFolder);

                // текущая платформа
                var platformName = platform == Platform.Android ? AndroidAliases : IosAliases;

                // папка подходящая для текущей платформы
                var packagePlatformFolder = GetIntersectionWithFolder(packagePlatformFolders, platformName);

                // если нет папки для платформы, тогда из ядра
                if (packagePlatformFolder == null)
                {
                    packagePlatformFolder = GetIntersectionWithFolder(packagePlatformFolders, PortableAliases);
                }

                // все dll внутри папки
                var files = Directory.GetFiles(packagePlatformFolder, "*.dll");

                var result = new List<ReferenceProject>();

                foreach (var file in files)
                {
                    var refProject = new ReferenceProject
                    {
                        Reference = Path.GetFileNameWithoutExtension(file),
                        HintPath = file.GetPathFromPackages()
                    };

                    result.Add(refProject);
                }

                return result;
            }
            catch (Exception)
            {
                return new List<ReferenceProject>();
            }
        }

        // Получить папку пакета относительно приоритета платформы
        private static string GetIntersectionWithFolder(string[] folders, string[] platforms)
        {
            foreach (var platform in platforms)
            {
                foreach (var folder in folders)
                {
                    var contains = Path.GetFileName(folder).StartsWith(platform, StringComparison.Ordinal);
                    if (contains)
                    {
                        return folder;
                    }
                }
            }

            return null;
        }
    }
}
