﻿using System;
using System.IO;
using System.Linq;

namespace Nugether
{
    public static class StringExtensions
    {
        // Уход на пять уровней назад
        public static string GetPathFromPackages(this string file)
        {
            var foldersArray = file.Split(Path.DirectorySeparatorChar).Reverse().Take(5).Reverse();
            return String.Join(@"\", foldersArray);
        }
    }
}
