﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using Nugether.Models;

namespace Nugether.Commands
{
    public class EditConfigCommand
    {
        /// <summary>
        /// Добавить пакеты в файл package.config.
        /// </summary>
        /// <param name="path">Путь до файла package.config.</param>
        /// <param name="packages">Список пакетов для установки.</param>
        public void Execute(string path, IList<Package> packages)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("Файл отсутствует", path);
            }

            var xmlContent = File.ReadAllText(path);

            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlContent);

            var packagesContainer = xmlDocument.LastChild;

            foreach (var package in packages)
            {
                SavePackage(xmlDocument, packagesContainer, package);
            }

            xmlDocument.Save(path);
        }

        private void SavePackage(XmlDocument document, XmlNode rootNode, Package package)
        {
            // if exists, then ignore
            foreach (XmlNode childPackageNode in rootNode.ChildNodes)
            {
                if (childPackageNode.Attributes["id"]?.Value == package.Id)
                {
                    return;
                }
            }

            var frameworkName = package.Platfrom == Platform.Android ? "monoandroid81" : "xamarinios10";

            var node = document.CreateElement("package");
            node.SetAttribute("id", package.Id);
            node.SetAttribute("version", package.Version);
            node.SetAttribute("targetFramework", frameworkName);

            rootNode.AppendChild(node);
        }
    }
}
