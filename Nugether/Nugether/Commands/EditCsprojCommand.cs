﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using Nugether.Models;

namespace Nugether.Commands
{
    public class EditCsprojCommand
    {
        /// <summary>
        /// Добавить пакеты в файл csproj.
        /// Добавляет ссылки на dll.
        /// </summary>
        /// <param name="path">Путь до файла csproj.</param>
        /// <param name="packages">Список пакетов для установки.</param>
        /// <param name="packagesFolder">Путь к папке packages.</param>
        public void Execute(string path, string packagesFolder, IList<Package> packages)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("Файл отсутствует", path);
            }

            var references = packages.Select(x => ReferenceUtils.GetReferences(packagesFolder, x.Id, x.Platfrom));
            var xmlContent = File.ReadAllText(path);

            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlContent);

            var nsmgr = new XmlNamespaceManager(xmlDocument.NameTable);
            nsmgr.AddNamespace("nm", "http://schemas.microsoft.com/developer/msbuild/2003");

            var referencesContainer = xmlDocument.SelectSingleNode("//nm:Project/nm:ItemGroup", nsmgr);
            foreach (var reference in references)
            {
                SaveReference(xmlDocument, referencesContainer, reference);
            }

            xmlDocument.Save(path);
        }

        private void SaveReference(XmlDocument document, XmlNode rootNode, List<ReferenceProject> references)
        {
            // todo need check on exists

            foreach (var reference in references)
            {
                var node = document.CreateElement("Reference");
                node.SetAttribute("Include", reference.Reference);

                var hintpath = document.CreateElement("HintPath");
                hintpath.InnerText = @"..\" + reference.HintPath;

                node.AppendChild(hintpath);

                rootNode.AppendChild(node);
            }
        }
    }
}
