﻿using System.Diagnostics;

namespace Nugether.Commands
{
    public class RestoreCommand
    {
        /// <summary>
        /// Восстановить пакеты (загрузить в папку packages)
        /// </summary>
        /// <param name="packagesFolder">Путь до папки packages.</param>
        /// <param name="packagesConfig">Путь до файла packages.config.</param>
        public void Execute(string packagesConfig, string packagesFolder) 
        {
            var process = Process.Start("nuget", $"restore {packagesConfig} -PackagesDirectory {packagesFolder}");
            process.WaitForExit();
        }

        /// <summary>
        /// Восстановить пакеты (загрузить в папку packages)
        /// </summary>
        /// <param name="solution">Path to Solution file.</param>
        public void Execute(string solution) 
        {
            var process = Process.Start("nuget", $"restore {solution}");
            process.WaitForExit();
        }
    }
}
