﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using NuGet;
using Nugether.Models;

namespace Nugether.Commands
{
    public class GetDependenciesCommand
    {
        private class InternalContainer
        {
            public IPackageRepository PrimaryRepository { get; set; }

            public IPackageRepository SecondaryRepository { get; set; }

            public FrameworkName FrameworkName { get; set; }

            public IPackage Package { get; set; }

            public bool PreRelease { get; set; }

            public int Level { get; set; }

            public bool Verbose { get; set; }
        }

        protected IPackageRepository LoymaxNuget { get; }
        protected IPackageRepository GeneralNuget { get; }
        protected FrameworkName IosFramework { get; }
        protected FrameworkName DroidFramework { get; }
        protected IList<IPackage> Packages { get; private set; }
        protected IList<string> Blacklist { get; private set; } = new List<string>();

        public GetDependenciesCommand()
        {
            this.IosFramework = new FrameworkName("Xamarin.iOS", new Version(1, 0));
            this.DroidFramework = new FrameworkName("MonoAndroid", new Version(8, 1));
            this.LoymaxNuget = PackageRepositoryFactory.Default.CreateRepository("https://nuget.loymax.net/nuget");
            this.GeneralNuget = PackageRepositoryFactory.Default.CreateRepository("https://www.nuget.org/api/v2/");
        }

        /// <summary>
        /// Какие пакеты игнорировать 
        /// </summary>
        /// <param name="blacklist">Черный список идентификаторов пакетов.</param>
        public void SetBlacklist(IList<string> blacklist)
        {
            this.Blacklist = blacklist;
        }

        /// <summary>
        /// Получить пакеты со всеми зависимостями.
        /// </summary>
        /// <returns>Пакеты.</returns>
        /// <param name="options">Параметры.</param>
        public List<Package> Execute(NugetOptions options)
        {
            this.Packages = new List<IPackage>();

            var version = new SemanticVersion(options.Version);
            var framework = options.Android ? this.DroidFramework : this.IosFramework;
            var foundedPackage = this.LoymaxNuget.FindPackage(options.Id, version, options.PreRelease, false);

            var packages = new List<IPackage>
            {
                foundedPackage
            };

            foreach (IPackage package in packages)
            {
                var container = new InternalContainer()
                {
                    PrimaryRepository = this.GeneralNuget,
                    SecondaryRepository = this.LoymaxNuget,
                    FrameworkName = framework,
                    Package = package,
                    PreRelease = options.PreRelease,
                    Level = 0,
                    Verbose = options.Verbose
                };

                this.GetDependencies(container);
            }

            var packagesList = this.Packages
                                   .Select(x => new Package
                                   {
                                       Id = x.Id,
                                       Version = x.Version.ToFullString(),
                                       Platfrom = GetPlatform(options.Android),
                                       Framework = framework,
                                   })
                                   .OrderBy(x => x.Id)
                                   .ToList();

            if (options.Verbose)
            {
                Console.WriteLine("===== Sorted output for " + framework.FullName);

                foreach (var pck in packagesList)
                {
                    Console.WriteLine(pck);
                }

                Console.WriteLine("===== Complete. Count = " + packagesList.Count);
            }

            this.Packages = null;

            return packagesList;
        }

        private void GetDependencies(InternalContainer container)
        {
            var hasPackage = this.Packages.Any(x => x.Id == container.Package.Id);
            var hasBlacklist = this.Blacklist.Any(x => x == container.Package.Id);

            if (!hasPackage && !hasBlacklist)
            {
                this.Packages.Add(container.Package);
            }

            if (container.Verbose)
            {
                Console.WriteLine("{0}{1}", new string(' ', container.Level * 3), container.Package);
            }

            foreach (PackageDependency dependency in container.Package.GetCompatiblePackageDependencies(container.FrameworkName))
            {
                IPackage subPackage = container.PrimaryRepository.ResolveDependency(dependency, container.PreRelease, false);
                if (subPackage == null)
                {
                    subPackage = container.SecondaryRepository.ResolveDependency(dependency, container.PreRelease, false);
                }

                if (subPackage != null)
                {
                    var subContainer = new InternalContainer()
                    {
                        PrimaryRepository = container.PrimaryRepository,
                        SecondaryRepository = container.SecondaryRepository,
                        FrameworkName = container.FrameworkName,
                        Package = subPackage,
                        PreRelease = container.PreRelease,
                        Level = container.Level + 1,
                        Verbose = container.Verbose
                    };

                    this.GetDependencies(subContainer);
                }
            }
        }

        private static Platform GetPlatform(bool isAndroid)
        {
            return isAndroid ? Platform.Android : Platform.iOS;
        }
    }
}