﻿namespace Nugether
{
    public class NugetOptions
    {
        public string SolutionPath { get; set; }

        public string Id { get; set; }

        public string Version { get; set; }

        public bool PreRelease { get; set; }

        public bool Android { get; set; }

        public bool Ios { get; set; }

        public bool Verbose { get; set; }

        public static NugetOptions Map(CmdOptions options)
        {
            return new NugetOptions
            {
                SolutionPath = options.SolutionPath,
                Id = options.Id,
                PreRelease = options.PreRelease,
                Verbose = options.Verbose,
                Version = options.Version
            };
        }
    }
}