﻿namespace Nugether.Models
{
    public class ReferenceProject
    {
        public string Reference { get; set; }

        public string HintPath { get; set; }
    }
}
