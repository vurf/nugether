﻿using System.Runtime.Versioning;

namespace Nugether.Models
{
    public class Package
    {
        public string Id { get; set; }

        public string Version { get; set; }

        public FrameworkName Framework { get; set; }

        public Platform Platfrom { get; set; }

        public override string ToString()
        {
            return $"{this.Id}, {this.Version}, {this.Platfrom}, {this.Framework}";
        }
    }
}
