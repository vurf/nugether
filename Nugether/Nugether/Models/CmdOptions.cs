﻿using CommandLine;

namespace Nugether
{
    public class CmdOptions
    {
        [Option('p', "path", Required = true, HelpText = "Путь к папке с решением")]
        public string SolutionPath { get; set; }

        [Option('i', "id", Required = true, HelpText = "Идентификатор пакета для установки")]
        public string Id { get; set; }

        [Option('v', "version", Required = true, HelpText = "Версия пакета")]
        public string Version { get; set; }

        [Option("prerelease", Default = false, HelpText = "Использовать пререлиз пакета")]
        public bool PreRelease { get; set; }

        // Omitting long name, defaults to name of property, ie "--verbose"
        [Option(Default = false, HelpText = "Выводить все сообщения в консоль.")]
        public bool Verbose { get; set; }
    }
}
